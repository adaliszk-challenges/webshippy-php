<?php

$database_name = @$argv[1] ?? 'database.sqlite';

$db = new PDO("sqlite:{$database_name}");
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$db->exec(<<<SQL
CREATE TABLE IF NOT EXISTS persons (
  id INTEGER PRIMARY KEY,
  name TEXT,
  email TEXT,
  phone TEXT,
  birthday DATE,
  driverLicense TEXT,
  hobby TEXT
);
SQL
);
