<?php
namespace WebShippy\PersonManager;

use PDO;
use ReflectionClass;


class Repository
{
    private $valueObject;

    private $table = 'persons';
    private $db;

    // @TODO: Interface használata ReflectionClass helyett
    public function __construct(string $databaseFile, ReflectionClass $valueObject)
    {
        $pdo = new PDO("sqlite:{$databaseFile}");
        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        $this->db = $pdo;
        $this->valueObject = $valueObject;
    }

    public function add(Person $person): bool
    {
        // @TODO: Rendes Query Builder és escape-elés
        $fields = implode(', ', $person::FIELDS); $places = ':'. implode(', :', $person::FIELDS);
        $statement = $this->db->prepare("INSERT INTO {$this->table} ({$fields}) VALUES ({$places})");

        foreach ($person::FIELDS as $attr_name) {
            $$attr_name = $person->$attr_name; // èrték kimentése, mert nem módosítható a pointer-en
            $statement->bindParam(":{$attr_name}", $$attr_name, PDO::PARAM_STR);
        }

        return (bool) $statement->execute();
    }

    public function save(Person $person): bool
    {
        // @TODO: Rendes Query Builder és escape-elés
        $set = '';
        foreach ($person::FIELDS as $attr_name) {
            if (!empty($set)) $set .= ', ';
            $set .= "{$attr_name} = :{$attr_name}";
        }
        $statement = $this->db->prepare("UPDATE {$this->table} SET {$set} WHERE id = :id");

        foreach ($person::FIELDS as $attr_name) {
            $$attr_name = $person->$attr_name; // èrték kimentése, mert nem módosítható a pointer-en
            $statement->bindParam(":{$attr_name}", $$attr_name, PDO::PARAM_STR);
        }

        $id = $person->id; // èrték kimentése, mert nem módosítható a pointer-en
        $statement->bindParam(':id', $id, PDO::PARAM_INT);

        return (bool) $statement->execute();
    }

    public function objects(): iterable
    {
        $fields = $this->valueObject->getConstant('FIELDS');
        $statement = $this->db->prepare('SELECT id,' . implode(',', $fields) . " FROM {$this->table};");
        $statement->execute();

        foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $values = array_values($row);
            yield $this->valueObject->newInstance(...$values);
        }
    }
}