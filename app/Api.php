<?php /** @noinspection PhpIncompatibleReturnTypeInspection */

namespace WebShippy\PersonManager;

use ReflectionClass;
use stdClass as object;
use Throwable;

class Api
{
    private $repository;
    private $person;

    // @TODO: IoC és Interface használata
    // @TODO: Interface használata ReflectionClass helyett
    public function __construct(Repository $repository, ReflectionClass $valueObject)
    {
        $this->repository = $repository;
        $this->person = $valueObject;
    }

    protected function get_object(object $requestData, int $id = null): Person
    {
        $data = [$id]; // ID mező
        foreach ($this->person->getConstant('FIELDS') as $field) {
            if (property_exists($requestData, $field)) $data[] = $requestData->$field;
        }

        /** @var Person $object */
        return $this->person->newInstance(...$data);
    }

    public function patch(object $requestData): array
    {
        try {
            $object = $this->get_object($requestData, $requestData->id);
            $this->repository->save($object);
            return [
                'data' => [$object],
                'errors' => [],
            ];
        } catch (Throwable $e) {
            http_response_code(500);
            return [
                'data' => [],
                'errors' => [
                    [
                        'code' => 500,
                        'title' => 'Hiba történt az adatok mentése közben!',
                        'detail' => $e->getMessage(),
                    ]
                ],
            ];
        }
    }

    // @TODO: JsonSerializable class legyen a kimenet
    public function put(object $requestData): array
    {
        try {
            $object = $this->get_object($requestData);
            $this->repository->add($object);
            return [
                'data' => [$object],
                'errors' => [],
            ];
        } catch (Throwable $e) {
            http_response_code(500);
            return [
                'data' => [],
                'errors' => [
                    [
                        'code' => 500,
                        'title' => 'Hiba történt az adatok mentése közben!',
                        'detail' => $e->getMessage(),
                    ]
                ],
            ];
        }
    }

    // @TODO: JsonSerializable class legyen a kimenet
    public function get(?object $requestData): array
    {
        try {
            /** @noinspection PhpParamsInspection */
            return [
                'data' => iterator_to_array($this->repository->objects()),
                'errors' => [],
            ];
        } catch (Throwable $e) {
            http_response_code(500);
            return [
                'data' => [],
                'errors' => [
                    [
                        'code' => 500,
                        'title' => 'Hiba történt az adatok mentése közben!',
                        'detail' => $e->getMessage(),
                    ]
                ],
            ];
        }
    }
}
