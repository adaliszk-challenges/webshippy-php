<?php
require "../vendor/autoload.php";

// Debug bekapcsolása
ini_set('display_errors', 'On');
error_reporting(-1);

// @TODO: Rendes iniializáló fájl használata
define('ROOT_PATH', realpath(dirname(dirname(__FILE__))));

$allowed_methods = ['GET', 'PATCH', 'POST', 'OPTIONS'];

// @TODO: Ezt valami IoC konténerben kellene kezelni
try {
    $person = new ReflectionClass(WebShippy\PersonManager\Person::class);
    $repository = new WebShippy\PersonManager\Repository(
        ROOT_PATH . DIRECTORY_SEPARATOR . 'database.sqlite',
        $person
    );
    $controller = new  WebShippy\PersonManager\Api($repository, $person);

    $input = file_get_contents('php://input');
    $request = json_decode($input);

    // @TODO: Rendes Router class használata
    switch ($_SERVER['REQUEST_METHOD']) {
        case "OPTIONS":
            return http_response([
                'data' => $allowed_methods,
                'errors' => [],
            ]);
        case "PATCH":
            // @TODO: Rendes HttpRequest class átadása amelyben van védelem is
            return http_response($controller->patch($request));
        case "PUT":
        case "POST":
            // @TODO: Rendes HttpRequest class átadása amelyben van védelem is
            return http_response($controller->put($request));
        case "GET":
            // @TODO: Rendes HttpRequest class átadása amelyben van védelem is
            return http_response($controller->get($request));
        default:
            http_response_code(405);
            return http_response([
                'data' => [],
                'errors' => [
                    [
                        'code' => 405,
                        'title' => 'Method not Allowed!',
                        'detail' => 'A hívott HTTP metódus nem engedélyezett!'
                    ]
                ],
            ]);
    }
} catch (Throwable $e) {
    http_response_code(500);
    return http_response([
        'data' => [],
        'errors' => [
            [
                'code' => 500,
                'title' => 'Valami hiba történt!',
                'detail' => $e->getMessage()
            ]
        ],
    ]);
}

function http_response(array $response): bool {
    global $allowed_methods;

    // @TODO: Rendes HttpResponse használata
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: ' . implode(', ', $allowed_methods));
    echo json_encode($response);

    return TRUE;
}
